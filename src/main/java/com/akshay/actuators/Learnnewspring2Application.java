package com.akshay.actuators;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Learnnewspring2Application {

	public static void main(String[] args) {
		SpringApplication.run(Learnnewspring2Application.class, args);
	}

}
