package com.akshay.actuators.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ActuatorsController {

    public ResponseEntity<String> HealthCheck(){


        return new ResponseEntity<>("hello", HttpStatus.OK);
    }


}
